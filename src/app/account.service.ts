import { LoggingService } from './logging.service';
import { Injectable, EventEmitter } from '@angular/core';
@Injectable() // it is metadata and mandtory to use where we want to inject service
export class AccountService {
  accounts = [
    {
      name: 'TestAccount',
      status: 'inactive'
    },
    {
      name: 'MasterAccount',
      status: 'active'

    },
    {
      name: 'Hidden Account',
      status: 'unknown'
    }
  ];
  statusUpdated = new EventEmitter<string>(); // new event which shows the status when triggered in another component
  constructor(private loggingService: LoggingService) {}
  addAccount(name1: string, status1: string) {
    // @ts-ignore
    this.accounts.push({name: name1, status: status1});
    this.loggingService.logStatusChanged(status);

  }
  updateStatus(id: number, status: string) {
    this.accounts[id].status = status;
    this.loggingService.logStatusChanged(status);
  }
}
