import { Component, Input, Output, EventEmitter } from '@angular/core';
import { LoggingService } from '../logging.service';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
 // providers: [LoggingService] // it's not manadatory to create instance of account here bcz it is already declared in its parent component
})
export class AccountComponent {
  @Input() account: {name: string, status: string};
  @Input() id: number;
 // @Output() statusChanged = new EventEmitter<{id: number, newStatus: string}>();
  constructor(private loggingService: LoggingService, private accountService: AccountService) {}

  onSetTo() {
    // this.statusChanged.emit({id: this.id, newStatus: status});
   // console.log('A server status changed, new status: ' + status);
   // this.loggingService.logStatusChanged(status);
    this.accountService.updateStatus(this.id, status);
    this.accountService.statusUpdated.emit(status); // event is emitted here which is defined in account service
  }
}
