import { Component, OnInit } from '@angular/core';
import { AccountService } from './account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
 // providers: [AccountService]
})
export class AppComponent implements OnInit {
  title = 'ServicePractice';
  accounts: {name: string, status: string}[] = []; // initialized it with empty array
  constructor(private accountService: AccountService) {}
  ngOnInit() {
   this.accounts = this.accountService.accounts; // getting access the same array as stored in the accountService
  }
 // accounts = [
   // {
   //   name: 'TestAccount',
    //  status: 'inactive'
    // },
   // {
   //   name: 'MasterAccount',
   //   status: 'active'

  //  },
  //  {
  //    name: 'Hidden Account',
   //   status: 'unknown'
  //  }
 // ];
 // onAccountAdded(newAccount: {name: string, status: string}) {
 //   this.accounts.push(newAccount);

 // }
 // onStatusChanges(updateInfo: {id: number, newStatus: string}) {
    // ts-ignore
 //   this.accounts[updateInfo.id].status = updateInfo.newStatus;

 }
