import { Component, Output, EventEmitter } from '@angular/core';
import { LoggingService } from '../logging.service';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
 // providers: [LoggingService] it's not manadatory to create instance of account here bcz it is already declared in its parent component
})
export class NewAccountComponent  {
//  @Output() accountAdded = new EventEmitter<{name: string, status: string}>();

  constructor(private logggingService: LoggingService,
              private accountService: AccountService) {
                // event statusUpdated is listened here
                this.accountService.statusUpdated.subscribe(
                   (status: string) => alert('New Status: ' + status) // here alert message is shown with new status updated
                );
  }
  onCreateAccount(accountName: string, accountStatus: string) {
    this.accountService.addAccount(accountName, accountStatus);
    this.logggingService.logStatusChanged(accountStatus);
    // this.accountAdded.emit({
     // name: accountName,
     // status: accountStatus
   // });
  }
}
